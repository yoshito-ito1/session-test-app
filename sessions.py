import logging
import routes
# import json
from webob import exc

from db import objects
from wsgi import Resource, Router


LOG = logging.getLogger(__name__)

DB_CLIENT = "memory"


class SessionController(object):
    def __init__(self):
        self.name = 'SessionController'
        self.db_client = objects.get_db_client()

    def index(self, req):
        pass

    def create(self, req, body):
        print("SessionController.create")
        print(body)
        sessiondata = objects.SessionData(self.db_client)
        userdata_id = body.get('user_id')
        if not userdata_id:
            raise exc.HTTPBadRequest
        sessiondata.create(userdata_id)

    def show(self, req):
        pass

    def delete(self, req):
        pass


class SessionRouter(Router):

    def __init__(self):
        mapper = routes.Mapper()
        resource = Resource(SessionController())

        super(SessionRouter, self).__init__(mapper, resource)

        ROUTE_LIST = (
            ('/sessions', {
                'GET': 'index',
                'POST': 'create',
            }),
            ('/sessions/{id}', {
                'GET': 'show',
                'DELETE': 'delete'
            }),
        )

        self._setup_routes(ROUTE_LIST)
