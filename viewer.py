def list_to_dict():
    pass


class Viewer(object):
    def __init__(self):
        super().__init__()

    def create(self):
        pass

    def show(self):
        pass

    @classmethod
    def index(self, data):
        if isinstance(data, list):
            return [list_to_dict(each_data) for each_data in data]
        else:
            raise RuntimeError

    def update(self):
        pass

    def delete(self):
        pass
