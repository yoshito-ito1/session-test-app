import logging
import routes
import json
from webob import exc

from db import objects
from wsgi import Resource, Router


LOG = logging.getLogger(__name__)

DB_CLIENT = "memory"


class UserController(object):
    def __init__(self):
        self.name = 'UserController'
        self.db_client = objects.get_db_client()

    def index(self, req):
        print("UserController.index")
        userdata_list = objects.UserData(self.db_client).index()
        out = [objects.to_dict(d) for d in userdata_list]
        print(out)

        return 200, json.dumps(out)

    def create(self, req, body):
        print("UserController.create")
        print(body)
        userdata = objects.UserData(self.db_client)
        # body = json.loads(req.body.decode())
        name = body.get('name')
        if not name:
            raise exc.HTTPBadRequest
        try:
            userdata.create(name=name)
        except Exception:
            raise RuntimeError(("Userdata requires 'id' to be created"))
        print(userdata.id)

        return 201, userdata.id

    def show(self, req, id):
        print("UserController.show")
        userdata = objects.UserData(self.db_client).show(id)
        if not userdata:
            LOG.warning(("Userdata %(id)s is not found") % {'id': str(id)})
            raise exc.HTTPNotFound
        out = objects.to_dict(userdata)
        print(out)

        return 200, json.dumps(out)

    def delete(self, req, id):
        print("UserController.delete")
        userdata = objects.UserData(self.db_client)
        if not userdata:
            LOG.warning(("Userdata %(id)s is not found") % {'id': str(id)})
            raise exc.HTTPNotFound
        userdata.delete(id)
        return 200, id


class UserRouter(Router):

    def __init__(self):
        mapper = routes.Mapper()
        resource = Resource(UserController())

        super(UserRouter, self).__init__(mapper, resource)

        ROUTE_LIST = (
            ('/users', {
                'GET': 'index',
                'POST': 'create',
            }),
            ('/users/{id}', {
                'GET': 'show',
                'DELETE': 'delete'
            }),
        )

        self._setup_routes(ROUTE_LIST)
