import json
import routes.middleware
import webob.dec
import webob.exc


def get_response(code=200, body=None):

    CODES = (200, 201, 202)
    CONTENT_TYPE = 'application/json'

    if code == 200:
        res = webob.exc.HTTPOk(text=body, content_type=CONTENT_TYPE)
    elif code == 201:
        res = webob.exc.HTTPCreated(text=body, content_type=CONTENT_TYPE)
    elif code == 202:
        res = webob.exc.HTTPAccepted(text=body, content_type=CONTENT_TYPE)
    else:
        raise RuntimeError(("Status code %(codes)s are supported") %
                           {'codes': str(CODES)})
    # if body:
    #     res.body = body

    return res


class Request(webob.Request):
    def __init__(self, environ, *args, **kwargs):
        super(Request, self).__init__(environ, *args, **kwargs)


'''
class Response(webob.Response):
    def __init__(self):
        super(Response).__init__()
'''


class Router(object):

    ROUTE_LIST = ()

    @classmethod
    def app_factory(cls, global_config, **local_config):
        return cls()

    def __init__(self, mapper, controller=None):
        self.map = mapper
        self.controller = controller
        self._router = routes.middleware.RoutesMiddleware(self._dispatch,
                                                          self.map)

    @webob.dec.wsgify
    def __call__(self, req):
        return self._router

    @staticmethod
    @webob.dec.wsgify(RequestClass=Request)
    def _dispatch(req):
        match = req.environ['wsgiorg.routing_args'][1]
        if not match:
            return webob.exc.HTTPNotFound()
        app = match['controller']
        return app

    def _setup_routes(self, ROUTE_LIST):
        for path, methods in ROUTE_LIST:
            self._setup_route(path, methods)

    def _setup_route(self, path, methods):
        for method, action in methods.items():
            self.map.connect(path,
                             conditions=dict(method=[method]),
                             controller=self.controller,
                             action=action)
        '''
        all_methods = ['HEAD', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE']
        missing_methods = [m for m in all_methods if m not in methods]
        if missing_methods:
            mapper.connect(path,
                           controller=self.controller,
                           action='reject',
                           conditions={'method': missing_methods})
        '''


class Resource(object):
    def __init__(self, controller):
        self.controller = controller

    @webob.dec.wsgify(RequestClass=Request)
    def __call__(self, request):
        action_args = self.get_action_args(request.environ)
        action_args.update(self.get_body(request.body))
        action = action_args.pop('action', None)
        code, body = self.dispatch(action, request, action_args)
        # return webob.Response(action_result)
        return get_response(code=code, body=body)

    def dispatch(self, action, request, kwargs):
        method = getattr(self.controller, action)
        return method(request, **kwargs)

    def get_action_args(self, request_environment):
        try:
            args = request_environment['wsgiorg.routing_args'][1].copy()
        except Exception:
            return {}
        try:
            del args['controller']
        except KeyError:
            pass
        return args

    def get_body(self, body):

        def _from_json(request):
            try:
                return json.loads(body.decode())
            except ValueError:
                raise RuntimeError("Cannot understand JSON")

        if not body:
            return {}

        return {'body': _from_json(body)}
