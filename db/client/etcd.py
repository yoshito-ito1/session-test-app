from copy import deepcopy
from datetime import datetime
import etcd3

import network_topology.conf as conf
from network_topology.db.client.abstract_db_client import AbstractDbClient


CONF = conf.conf()


# https://pypi.org/project/etcd3/
class EtcdClient(AbstractDbClient, etcd3.Etcd3Client):

    def __init__(
            self,
            host='127.0.0.1',
            port=2379,
            ca_cert=None,
            cert_key=None,
            cert_cert=None,
            timeout=None,
            user=None,
            password=None,
            grpc_options=None):
        super().__init__(
            host=host,
            port=port,
            ca_cert=ca_cert,
            cert_key=cert_key,
            cert_cert=cert_cert,
            timeout=timeout,
            user=user,
            password=password,
            grpc_options=grpc_options)

    def create(self, dataclass):
        dataclass.created_at = datetime.now()

        out, nested_dataclass = self._create_dict_for_etcd(dataclass)

        type = dataclass.type
        uuid = dataclass.uuid
        # Need to convert to str without ' ' or ':' for directory
        created_at = dataclass.created_at.strftime(DIRECTORY_TIMESTAMP)

        for key, value in out.items():
            super().put('/'.join(['', type, uuid, created_at, key]), value)

        # Data structure is like:
        # /Building/<UUID>/2020-12-30_11-40-15/address 3-9-11, Midori-cho, ...
        #                                     /company NTT
        #                                     /created_at 2020-12-30 11:40:15
        #                                     /deleted_at None
        #                                     /department NTT NS Lab
        #                                     /floors 6eece774-48e0-499f-...
        #                                     /name Tokujitsutou
        #                                     /type Building
        #                                     /updated_at None
        #                                     /uuid 313a7199-2c2e-426d-...
        # /Floor/<UUID>/2020-12-30_11-40-15/created_at 2020-12-30
        # ...

        # call self.create() for nested dataclass
        while nested_dataclass:
            self.create(nested_dataclass.pop())

    def read(self, type='', uuid=''):
        # TODO(yoshito-ito): Need to get only latest data.
        # Getting all old data of its UUID may introduce late response.
        # This logic assume to get newer data later, and the value is
        # updated by the newest data.
        res = super().get_prefix('/'.join(['', type, uuid]),
                                 sort_order='ascend')

        # Data structure is like:
        # /Building/<UUID>/2020-12-30_11-40-15/address 3-9-11, Midori-cho, ...
        #                                     /company NTT
        #                                     /created_at 2020-12-30 11:40:15
        #                                     /deleted_at None
        #                                     /department NTT NS Lab
        #                                     /floors ['6eece774-48e0-499f-...
        #                                     /name Tokujitsutou
        #                                     /type Building
        #                                     /updated_at None
        #                                     /uuid 313a7199-2c2e-426d-...
        #                 /2020-12-30_11-40-25/address 3-9-11, Midori-cho, ...
        #                 ...
        #                                     /updated_at 2020-12-30 11:40:25

        out = {}
        for data in res:
            key = data[-1].key.decode()
            value = data[0].decode()

            # Make key to have only native member variable
            key = key.split('/')[-1]

            value = self._convert_type_from_str(key, value)
            if value:
                out[key] = value
        return out

    def update(self, dataclass):
        # TODO(yoshito-ito): May be better to use etcd native function.
        # etcd can store previous values, but this logic assume to create
        # new directory for each updated recode.
        dataclass.updated_at = datetime.now()

        out, nested_dataclass = self._create_dict_for_etcd(dataclass)

        type = dataclass.type
        uuid = dataclass.uuid
        # Need to convert to str without ' ' or ':' for directory
        updated_at = dataclass.updated_at.strftime(DIRECTORY_TIMESTAMP)

        for key, value in out.items():
            super().put('/'.join(['', type, uuid, updated_at, key]), value)

        # call self.update() for nested dataclass
        while nested_dataclass:
            self.update(nested_dataclass.pop())

    def delete(self, dataclass):
        dataclass.deleted_at = datetime.now()

        out, nested_dataclass = self._create_dict_for_etcd(dataclass)

        type = dataclass.type
        uuid = dataclass.uuid
        # Need to convert to str without ' ' or ':' for directory
        deleted_at = dataclass.deleted_at.strftime(DIRECTORY_TIMESTAMP)

        for key, value in out.items():
            super().put('/'.join(['', type, uuid, deleted_at, key]), value)

        # call self.delete() for nested dataclass
        while nested_dataclass:
            self.update(nested_dataclass.pop())

    def destroy(self, dataclass):

        _, nested_dataclass = self._create_dict_for_etcd(dataclass)

        type = dataclass.type
        uuid = dataclass.uuid
        super().delete_prefix('/'.join(['', type, uuid]))

        # call self.destroy() for nested dataclass
        while nested_dataclass:
            self.destroy(nested_dataclass.pop())

    def get_list(self, type):
        out = []
        res = super().get_prefix('/'.join(['', type]),
                                 sort_order='ascend')
        previous_uuid = ''
        out_dict = {}
        for data in res:
            key = data[-1].key.decode()
            value = data[0].decode()

            # Make key to have only native member variable
            simple_key = key.split('/')[-1]

            # Get UUID from key:
            # ['', 'PortLink', '2b98347f-0d11-49c4-9de4-59c3acb46b81',
            #   '2021-01-02_22-19-51', 'uuid']
            uuid = key.split('/')[2]
            if uuid != previous_uuid:
                out.append(deepcopy(out_dict))

            value = self._convert_type_from_str(simple_key, value)
            if value:
                out_dict[simple_key] = value
            previous_uuid = uuid
        # Need to append the last dict.
        out.append(out_dict)

        # Remove the first empty dict.
        out = out[1:]

        # The logic below is the simplest but poor performance
        # because too many read() access to etcd.
        # It's tricky but better to get data only once.
        #
        # uuid_list = self.get_uuid_list()
        # for uuid in uuid_list:
        #     out.append(self.read(uuid))

        return out

    @staticmethod
    def _create_dict_for_etcd(dataclass):
        out = {}
        nested = []
        for key, value in vars(dataclass).items():
            if isinstance(value, (str, int)) or (value is None):
                out[key] = str(value)
            elif isinstance(value, datetime):
                out[key] = value.strftime(DEFAULT_TIMESTAMP)
            else:
                # Store "UUID" of nested object
                if isinstance(value, list):
                    nested_uuids = []
                    for nested_dataclass in value:
                        nested_uuids.append(nested_dataclass.uuid)
                        nested.append(nested_dataclass)
                    # etcd doesn't support list()
                    # out[key] = ','.join(nested_uuids)
                    out[key] = str(nested_uuids)
                else:
                    nested.append(value)
                    out[key] = value.uuid
        return out, nested

    def _convert_type_from_str(self, key, str_value):
        # Get list() from str() like '[<UUID#1>,<UUID#2>,<UUID#3>]'
        if '[' in str_value and ']' in str_value:
            # Remove '[' and ']', and get list() from str().
            # Use strip(), or you'll get ["'hoo'", ...].
            if str_value == '[]':
                return None
            nested_uuids = [n.strip("' ") for n in str_value[1:-1].split(',')]
            return [self.read(DATA_TYPES.get(key), nested_uuid)
                    for nested_uuid in nested_uuids]

        # etcd stores only str type so needs to convert 'None' to None.
        if str_value == 'None':
            return None

        # etcd stores only str type so needs to convert str(int) to int().
        if key in INT_MEMBERS:
            return int(str_value)

        return str_value
