import logging

from db.client.abstract_db_client import AbstractDbClient


LOG = logging.getLogger(__name__)


class MemoryDbClient(AbstractDbClient):
    def __init__(self):
        self.data_list = []
        self.data_dict = {}
        self._deleted_data_list = []

    def create(self, obj):
        self.data_list.append(obj)
        if hasattr(obj, 'id'):
            self.data_dict[obj.id] = obj
        else:
            LOG.warning(("%(obj)s doesn't have 'id' attribute") %
                        {'obj': str(type(obj))})

    def show(self, id):
        data = self.data_dict.get(id)
        if not data:
            LOG.warning(("No data found for %(id)s") %
                        {'id': str(id)})
        return data

    def index(self):
        return self.data_list

    def update(self, obj):
        pass

    def delete(self, id):
        data = self.data_dict.get(id)
        if data:
            self.data_dict.pop(id)
            self.data_list.remove(data)
            self._deleted_data_list.append(data)

        else:
            LOG.warning(("No data found for %(id)s") %
                        {'id': str(id)})
