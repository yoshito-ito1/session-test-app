from abc import ABC, abstractmethod


class AbstractDbClient(ABC):

    @abstractmethod
    def create(self, obj):
        raise NotImplementedError()

    @abstractmethod
    def show(self, id):
        raise NotImplementedError()

    @abstractmethod
    def index(self):
        raise NotImplementedError()

    @abstractmethod
    def update(self, obj):
        raise NotImplementedError()

    @abstractmethod
    def delete(self, id):
        raise NotImplementedError()
