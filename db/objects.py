from abc import abstractmethod
import copy
import uuid
from datetime import datetime
# from db.client.abstract_db_client import AbstractDbClient

from db.client.memory import MemoryDbClient


DB_CLIENT = "memory"
SUPPORTED_DB_CLIENT = ("memory")

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S.%f'

SESSION_TIMER = 60


def get_db_client(db_client="memory"):
    if db_client not in SUPPORTED_DB_CLIENT:
        raise RuntimeError
    else:
        return MemoryDbClient()


def create_uuid():
    return str(uuid.uuid4())


def to_dict(obj):

    def drop_private_members(obj_dict):
        out = copy.deepcopy(obj_dict)
        for member in obj_dict.keys():
            if member.startswith('_'):
                out.pop(member)
        return out

    if not obj:
        return {}

    return drop_private_members(vars(obj))


class AbstractData(object):
    def __init__(self, db_client):
        self.created_at = None
        self.deleted_at = None
        self._db_client = db_client

    @abstractmethod
    def create(self):
        raise NotImplementedError

    @abstractmethod
    def show(self):
        raise NotImplementedError

    @abstractmethod
    def index(self):
        raise NotImplementedError

    @abstractmethod
    def update(self):
        raise NotImplementedError

    @abstractmethod
    def delete(self):
        raise NotImplementedError


class UserData(AbstractData):
    def __init__(self, db_client):
        super(UserData).__init__(self, db_client)

    def create(self, name):
        self.id = create_uuid()
        self.created_at = datetime.now().strftime(DATETIME_FORMAT)
        self.name = name
        self._db_client.create(self)
        return self.id

    def show(self, id):
        return self._db_client.show(id)

    def index(self):
        return self._db_client.index()

    def update(self):
        pass

    def delete(self, id):
        return self._db_client.delete(id)


class SessionData(AbstractData):
    def __init__(self, db_client):
        self.timer = SESSION_TIMER
        super(SessionData).__init__(self, db_client)

    def create(self, userdata_id):
        pass

    @classmethod
    def show(self, id):
        pass

    @classmethod
    def index(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass
