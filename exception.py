class BaseException(Exception):

    message = "Unknown exception."

    def __init__(self, message=None, **kwargs):
        if not message:
            try:
                message = self.message % kwargs
            except Exception:
                message = self.message
        self.msg = message
        super(BaseException, self).__init__(message)

    def __str__(self):
        return self.msg


class NotSupportedDbClient(BaseException):
    message = "%(db_client_type)s DB Client is not supported."


class DuplicateCreateOperation(BaseException):
    message = "Create operation failed due to UUID duplication."
