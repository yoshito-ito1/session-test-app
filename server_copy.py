import multiprocessing

import eventlet
from eventlet import wsgi


def hello_handler(env, start_response):
    start_response('200 OK', [('Content-Type', 'text/plain')])
    return ['Hello, World!\r\n']


def wsgi_serve(server_socket, handler):
    wsgi.server(server_socket, handler)


if __name__ == '__main__':
    server_socket = eventlet.listen(('localhost', 8080))
    for _ in range(2):
        process = multiprocessing.Process(target=wsgi_serve,
                                          args=(server_socket, hello_handler))
        process.daemon = True
        process.start()
    wsgi_serve(server_socket, hello_handler)
