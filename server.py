import logging
# import os
import signal
import socket
import sys
import time
# from multiprocessing import Pool

import eventlet.wsgi

# from sessions import SessionRouter


PORT = '8080'
HOST = '127.0.0.1'

WORKERS = 1
MAX_HEADER_LINE = 16384
NUM_THREADS = 10
MAX_CONNECTION = 4096
RETRY_UNTIL_WINDOW = 5
BACKLOG = 4096
TCP_KEEPIDLE = 600

LOG = logging.getLogger(__name__)


class Server(object):
    def __init__(self, name, num_threads=None):
        eventlet.wsgi.MAX_HEADER_LINE = MAX_HEADER_LINE
        self.num_threads = num_threads or NUM_THREADS
        self.pool = eventlet.GreenPool(1)
        self.child_pool = None
        self.name = name
        self._server = None
        self.children = set()

    def _get_socket(self, host, port, backlog):
        bind_addr = (host, port)
        try:
            info = socket.getaddrinfo(bind_addr[0],
                                      bind_addr[1],
                                      socket.AF_UNSPEC,
                                      socket.SOCK_STREAM)[0]
            family = info[0]
            bind_addr = info[-1]

        except Exception:
            LOG.exception("Unable to listen on %(host)s:%(port)s",
                          {'host': host, 'port': port})
            sys.exit(1)

        sock = None
        retry_until = time.time() + RETRY_UNTIL_WINDOW
        while not sock and time.time() < retry_until:
            try:
                sock = eventlet.listen(bind_addr,
                                       backlog=backlog,
                                       family=family)
            except socket.error as err:
                LOG.exception("Socket error: %s", err)

        if not sock:
            raise RuntimeError(("Could not bind to %(host)s:%(port)s "
                               "after trying for %(time)d seconds") %
                               {'host': host,
                                'port': port,
                                'time': RETRY_UNTIL_WINDOW})
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)

        if hasattr(socket, 'TCP_KEEPIDLE'):
            sock.setsockopt(socket.IPPROTO_TCP,
                            socket.TCP_KEEPIDLE,
                            TCP_KEEPIDLE)

        return sock

    def _single_run(self, application, socket):
        eventlet.wsgi.server(socket, application,
                             custom_pool=self.child_pool, log=LOG)

    '''
    def _run(self, application, socket):
        eventlet.wsgi.server(socket, application,
                             max_size=self.num_threads, log=LOG)

    def run_child_bk(self):
        pid = os.fork()
        if pid == 0:
            signal.signal(signal.SIGTERM, signal.SIG_DFL)
            signal.signal(signal.SIGINT, signal.SIG_IGN)
            self._run()
            sys.exit(0)
        else:
            LOG.info('Started child %s', pid)
            self.children.add(pid)

    def run_child(self, application):
        with Pool(WORKERS) as p:
            p.map(self._run,
                  [(application, self._socket) for i in range(WORKERS)])
        # p = Process(target=self._run, daemon=True)
        # p.start()

    def kill_children(self):
        signal.signal(signal.SIGTERM, signal.SIG_IGN)
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        os.killpg(0, signal.SIGTERM)
    '''

    def start(self, application, port=PORT, host=HOST):
        self._host = host
        self._port = port
        backlog = BACKLOG

        self._socket = self._get_socket(self._host,
                                        self._port,
                                        backlog=backlog)
        LOG.debug("Got socket at " + host + ':' + port)

        self.child_pool = eventlet.GreenPool(size=self.num_threads)
        if WORKERS == 1:
            self._server = self.child_pool.spawn(self._single_run, application,
                                                 self._socket)
            return

        elif WORKERS > 1:
            '''
            while len(self.children) < WORKERS:
                self.run_child(application)
            '''
            raise RuntimeError(("More than 1 WORKERS is not supported yet"))
        else:
            raise RuntimeError(("WORKERS should be positive"))

        LOG.debug("Spawned application as a thread in GreenThread pool")
        # self.child_pool.waitall()
        signal.signal(signal.SIGTERM, self.kill_children)
        signal.signal(signal.SIGINT, self.kill_children)

    def wait(self):
        self._server.wait()
        # self.child_pool.waitall()
        # self.pool.waitall()

    def stop(self):
        eventlet.kill(self._server)


'''
def app_factory(global_config, **local_config):
    return SessionRouter.app_factory(global_config, **local_config)
'''


def pipeline_factory(loader, global_config, pipeline):
    pipeline = pipeline.split()
    filters = [loader.get_filter(n) for n in pipeline[:-1]]
    app = loader.get_app(pipeline[-1])
    filters.reverse()
    for filter in filters:
        app = filter(app)
    return app


def run_server():
    from paste import deploy
    app = deploy.loadapp('config:api.ini', relative_to='.')
    LOG.debug("Loaded api.ini")

    try:
        server = Server('SessionTestApp')
        LOG.debug("Created Server() instance")
        server.start(app, port=PORT, host=HOST)
        LOG.info("Started Server() instance")
        server.wait()
    except (KeyboardInterrupt, SystemExit):
        LOG.exception("KeyboardInterrupt!!!")
        # server.stop()
    except RuntimeError as e:
        LOG.error("RuntimeError: " + str(e))
        sys.exit(("ERROR: %s") % e)


def run_server1():
    from paste import httpserver
    from paste import deploy
    httpserver.serve(deploy.loadapp('config:api.ini', relative_to='.'),
                     host='127.0.0.1', port='8080')


if __name__ == '__main__':
    run_server()
    # run_server1()
